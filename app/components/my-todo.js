import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  classNameBindings: ['todo.isCompleted:completed', 'isEditing:editing'],

  isCompleted: Ember.computed.oneWay('todo.isCompleted'),
  title: Ember.computed.oneWay('todo.title'),

  init() {
    this._super(...arguments);
    this.set('isEditing', false);
  },

  actions: {
    editTodo() {
      this.set('isEditing', true);
    },

    removeTodo() {
      var todo = this.get('todo');

      todo.deleteRecord();
      todo.save();
    },

    save() {
      this.set('isEditing', false);
      this.get('todo').save();
    },

    isCompletedChange(checked) {
      // this.sendAction('actionPatchTodo', this.get('todo'), 'isCompleted', checked)
      var todo  = this.get('todo')
      todo.set('isCompleted', checked)
      this.set('isEditing', false);
      todo.save();
    }
  },
});
