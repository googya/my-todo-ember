import Ember from 'ember';

export default Ember.Controller.extend({

model: function(){
  return this.store.filter('todo', function(todo) {
    return !todo.get('isCompleted');
  });
}

})
