import Ember from 'ember';

export default Ember.Controller.extend({

  actions: {
    createTodo: function() {
      var title = this.get('newTitle');

      if (!title.trim()) { return }

      var todo = this.store.createRecord('todo', {
        title: title,
        isCompleted: false
      })

      this.set('newTitle', '')
      todo.save()
    },

    clearCompleted: function(){
      var completed = this.get('completed')
      completed.invoke('destroyRecord')
    }
  },


  remaining: function() {
    return  this.model.filterBy('isCompleted', false)
  }.property('model.@each.isCompleted'),

    completed: function() {
      return  this.model.filterBy('isCompleted', true)
    }.property('model.@each.isCompleted')



});
